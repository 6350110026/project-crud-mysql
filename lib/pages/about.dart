import 'package:flutter/material.dart';
import 'package:path/path.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column();
  }
}

class About extends StatelessWidget {
  const About({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('ABOUT'),
        backgroundColor: Colors.blue,
      ),
      body: Column(
        children: [
          SizedBox(height: 10,),
          CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage("assets/images/one.jpg"),
          ),
          SizedBox(height: 20),
          name(
            text: Text(
              "นาย ธรรมสถิตย์ แสงจันทร์ 6350110026 ICM",
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(height: 50),
          CircleAvatar(
            radius: 100,
            backgroundImage: AssetImage("assets/images/two.png"),
          ),
          SizedBox(height: 20),
          name(
            text: Text(
              "นาย ศุทธิพัฒน์ วัฒนสิน 6350110028 ICM",
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }
}

class name extends StatelessWidget {
  const name({Key? key, required this.text}) : super(key: key);

  final Text text;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: FlatButton(
        padding: EdgeInsets.all(20),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        color: Colors.blue[200],
        onPressed: () {},
        child: Row(
          children: [
            Image.asset(
              "assets/images/user.png",
              width: 35,
            ),
            SizedBox(width: 30),
            Expanded(
              child: text,
            ),
          ],
        ),
      ),
    );
  }
}

class image extends StatelessWidget {
  const image({Key? key, required this.assetImage}) : super(key: key);

  final AssetImage assetImage;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: SizedBox(
        height: 115,
        width: 115,
        child: CircleAvatar(
          backgroundImage: assetImage,
        ),
      ),
    );
  }
}
